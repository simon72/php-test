<?php

use Symfony\Component\HttpFoundation\JsonResponse;
use App\Services\PokemonService;

require __DIR__ . '/../../src/bootstrap.php';

$name = $_GET['name'] ?? null;
$page = $_GET['page'] ?? 1;
$perPage = $_GET['perPage'] ?? null;
$action = $_GET['action'] ?? 'get-all';


$pokemon = new PokemonService();

if($action == 'get-one'){
    $data = $pokemon->get($name);
} else {
    $data = $pokemon->all($name, $page, $perPage);

}




$response = new JsonResponse();

$response
    ->setData($data)
    ->send();
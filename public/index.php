<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Pokédex</title>
<link rel="stylesheet" href="/css/app.css">
<link rel="stylesheet" href="/css/pokemon-font.css">
</head>
<body>
    <div id="app">
        <pokedex></pokedex>
    </div>
<script src="/js/app.js"></script>
</body>
</html>
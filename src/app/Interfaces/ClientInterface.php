<?php

namespace App\Interfaces;

interface ClientInterface {

    /**
     * send a request to server
     *
     * @param string $endpoint
     * @return Array
     */
    public function sendRequest($endpoint);
}
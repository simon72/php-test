<?php

namespace App;

class Paginator {

    private $items;

    public function __construct($items)
    {
        $this->items = collect($items);
    }

    /**
     * Simple paginatin of a collection
     *
     * @param integer $page
     * @param integer $perPage
     * @return Array
     */
    public function paginate(int $page = 1, int $perPage = 20)
    {
        $page = $page < 0 ? 1 : $page;

        $total = $this->items->count();
        $offset = $perPage * ($page - 1);

        $data = $this->items->slice($offset, $perPage);

        return [
            'data' => $data,
            'current_page' => $page,
            'per_page' => $perPage,
            'total' => $total,
        ];

    }

}
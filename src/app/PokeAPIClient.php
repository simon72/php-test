<?php

namespace App;

use GuzzleHttp\Client;
use App\Interfaces\ClientInterface;
use App\Exceptions\ResourceNotFoundException;
use Symfony\Component\Cache\Simple\FilesystemCache;

class PokeAPIClient implements ClientInterface {

    
    private $baseUrl = 'https://pokeapi.co/api/v2/';
    private $client;
    private $cache;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => $this->baseUrl,
            'verify' => false
        ]);

        $this->cache = new FilesystemCache(
            'pokemonStore',
            0,
            '/src/app/cache'
        );
    }


    /** 
     * get a list of pokemon from the api
     */
    public function getList($name = '')
    {
        return collect($this->sendRequest('pokemon')['results'])
            ->pluck('name')
            ->map(function($pokemon, $key){
                $id = $key + 1;
                return [
                    "id" => $id,
                    "name" => ucwords($pokemon),
                    "image_url" => $this->getImageForPokemon($id)
                ];
            })
            ->filter(function($pokemon) use ($name){

                if($name == ''){
                    return true;
                }
                // return if letters are in name
                return strstr( strtolower($pokemon['name']) , $name );
            })
            ->toArray();
    }

    /**
     * get the image url for the given pokemon
     *
     * @param int $id
     * @return string
     */
    private function getImageForPokemon(int $id){

        // pokemon above this index have no images on the PokeApi
        if($id >= 803){
            return '';
        }

        return "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/{$id}.png";
    }

    /**
     * get pokemon by name or id
     *
     * @param string $idOrName
     * @return void
     */
    public function pokemon($idOrName){
        return $this->sendRequest("pokemon/{$idOrName}");
    }


    /**
     * clear the cache
     *
     * @return void
     */
    public function clearCache(){
        $this->cache->clear();
    }

    /**
     * @param string $endpoint
     * @return mixed
     * @throws ResourceNotFoundException
     */
    public function sendRequest($endpoint)
    {

        $cache_key = str_replace('/', '_', $endpoint);

        // check the cache for the requested data
        if($this->cache->has($cache_key)){
            return $this->cache->get($cache_key);
        }

        try {
            $response = $this->client->get($endpoint);
        } catch (\Exception $e){
            throw new ResourceNotFoundException;
        }
        
        $data = json_decode($response->getBody(), true);

        $this->cache->set($cache_key, $data);

        return $data;
    }


}
<?php

namespace App\Services;

use App\PokeAPIClient;
use App\Paginator;


class PokemonService {

    protected $client;

    public function __construct()
    {
        $this->client = new PokeAPIClient();
    }


    /**
     * get all pokemon and paginate
     *
     * @param string $name
     * @param integer $page
     * @param integer $perPage
     * @return array
     */
    public function all(string $name, int $page, int $perPage = 20){
        
        $pokemon = $this->client->getList($name);

        return (new Paginator($pokemon))->paginate($page, $perPage);
        
    }

    /**
     * get a pokemon by name
     *
     * @param string $name
     * @return void
     */
    public function get(string $name){

        return [
            'data' => $this->client->pokemon($name)
        ];
    
    }


}
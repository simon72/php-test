<?php

namespace Tests\Unit;

use App\PokeAPIClient;
use PHPUnit\Framework\TestCase;

class PokeAPIClientTest extends TestCase {

    /** @test */
    public function client_can_get_pokemon_by_name(){
        
        $client = new PokeAPIClient();

        // get bulbasaur
        $bulbasaur = $client->pokemon('bulbasaur');

        $this->assertEquals('bulbasaur', $bulbasaur['name']);
    
    }

    /** @test */
    public function client_can_get_all($data){
        
    }
}